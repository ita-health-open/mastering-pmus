## Computational Simulator

To create new waveforms, just select the number of samples in waveform_generator.py and run it's python code.

## Train CNN

To train CNN, just run the code train_neural_networks.py

## Test performance with ASL

To test the performance using ASL dataset, just run main.py
